<?php
/******************************************************************************
 *
 * COPYRIGHT (C) 2009 SICOM Systems Inc
 * 4140 Skyron Drive
 * Doylestown, PA 18902
 * 215-489-2500, FAX: 215-489-2769
 *
 * $Id: database_connector.php,v 1.7 2010/01/12 21:39:36 mroth Exp $
 *
 ******************************************************************************/
require_once('siteunique.php');
require_once('logger.php');


class SicomDatabase
{
	var $_host;
	var $_user;
	var $_password;
	var $_database;
	var $_connection = null;

	public function __construct()
	{
		$this->_host = $GLOBALS['__sicom__host'];
		$this->_user = $GLOBALS['__sicom__user'];
		$this->_password = $GLOBALS['__sicom__password'];
		$this->_database = $GLOBALS['__sicom__database'];
	}

//CALL STATICALLY, like $db = SicomDatabase::getSicomDatabase();
	function getSicomDatabase()
	{
		global $__sicom_database;
		return $__sicom_database;
	}

	function getHost()
	{ return $this->_host; }

	function getUser()
	{ return $this->_user; }

	function getPassword()
	{ return $this->_password; }

	function getDatabase()
	{ return $this->_database; }

	//Returns boolean true if connection made, false otherwise.
	function connect()
	{
		$tresult = false;
		if ($this->_connection)
		{
			$tresult = true; //Already have a connection, don't get another
		}
		else
		{
			$this->_connection = @mysql_connect($this->_host, $this->_user, $this->_password);
			if ($this->_connection != NULL)
			{
				$tresult = mysql_select_db($this->_database, $this->_connection);
				if (!$tresult)
				{
					log_fatal("Failed to select Database: {$this->_database}");
				}
			}
			else
			{
				log_fatal("Failed to open Database connection for {$this->_user}@{$this->_host}");
			}
		}

		return $tresult;
	}

	function getConnection()
	{ return ($this->connect() ? $this->_connection : null); }


	function destroy()
	{
		if ($this->_connection)
		{
			mysql_close($this->_connection);
			$this->_connection = NULL;
		}
	}
	
	public function __destruct() {
		sicom_database_close();
	}
	
}	// *** End Class: SicomDatabase ***


function sicom_database_close()
{
	global $__sicom_database;
	if ($__sicom_database)
	{
		$__sicom_database->destroy();
	}

	unset($__sicom_database);
}

$__sicom_database = new SicomDatabase();
//$__sicom_database = new SicomDatabase();

?>
