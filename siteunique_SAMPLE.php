<?php
/**
 * @copyright (C) 2004-2017 SICOM Systems Inc
 *            4434 Progress Meadow Drive
 *            Doylestown, PA 18902
 *            215-489-2500, FAX: 215-489-2769
 */

/* System */
$__sicom__mode = ''; // E.g: dev/test/prod
// We can add this as a suffix when include JS and CSS files to discard cached versions in the browsers.
// E.g: 'script.js?v='. $__sicom__utopia_version;
$__sicom__utopia_version = ''; // E.g: 20130918.1
$__sicom__upgrade_in_progress = false;
$__sicom__tmp_dir = ''; // Directory for temporary files. Should be hidden from the web. E.g.: /tmp/utopia

/* Webserver */
// Reports we are sending by e-mail, pointing to a particular page of Utopia need the absoulte URL.
$__sicom__webserver_protocoll = 'http://'; // E.g: https:// or http://
$__sicom__webserver_host = 'gsalamon.staff'; // E.g: utopia.sicomasp.com or username.staff
$__sicom__webserver_base_dir = '/~gsalamon/utopia/'; // E.g: /~username/utopia/ or just a simple slash / if Utopia installed in the root of the virtualhost.

/* Dataobject Stuff */
$__sicom__user = "utopia";
$__sicom__password = "utopia_twinkletoes";
$__sicom__database = "utopia";
$__sicom__host = "localhost";

/* Security */
$__sicom__active_directory_group = 'utopiatech';
$__sicom__active_directory_base_dn = 'CN=Users, DC=sicom, DC=local';
$__sicom__active_directory_domain = 'sicom';
$__sicom__active_directory_controller_urls = array('ldaps://controller1.sicom.com', 'ldaps://controller2.sicom.com', 'ldaps://192.168.5.101');

/* Logging */
$__sicom__log_path = ""; // Base directory of all the log files of Utopia. E.g.: /data/log/
$__sicom__log_name = ""; // Filename of the general log. E.g.: utopia.log
$__sicom__log_flags = "11111111111111111111111111111111"; // Logging level of the general log.

/* Archiving */
$__sicom__archive_path = ''; // Specify a folder to store archives. E.g: /home/utopia/archive/

/* Comet and STOMP Servers */
$__sicom__stomp_alert_email_from = ''; // E.g: user@sicom.com
$__sicom__stomp_alert_email_to = ''; // E.g: user@sicom.com

// For development (uses X-port comet with local STOMP Server)
// NOTE: You should run the following from the ~/public_html/utopia dir
//    while [ 1 ]; do php stomp_heartbeat.php && sleep 60 ; done
// If you do not, the client will try to reconnect every minute or so.
$__sicom__stomp_server = "localhost";
$__sicom__stomp_port = 12000;
$__sicom__comet_subdomain = "";
$__sicom__comet_port = 8080;

// For development (uses X-domain comet with other box serving comet and STOMP)
// NOTE: Use /etc/hosts to point utopiadev.sicomasp.com at your box
//       and comet1.utopiadev.sicomasp.com at the other box
// $__sicom__stomp_server = "comet1.utopiadev.sicomasp.com";
// $__sicom__stomp_port = 12000;
// $__sicom__comet_subdomain = "comet1";
// $__sicom__comet_port = 80;  // We get set to 443 for SSL, don't worry

// For testing (uses X-subdomain comet with STOMP Server @ comet1.utopiademo.sicomasp.com)
// $__sicom__comet_subdomain = "comet1";
// $__sicom__stomp_server = "comet1.utopiademo.sicomasp.com";
// $__sicom__stomp_port = 12000;
// $__sicom__comet_port = 80;  // We get set to 443 for SSL, don't worry

// For production (uses X-subdomain comet STOMP Server @ comet1.utopia.sicomasp.com)
// $__sicom__comet_subdomain = "comet1";
// $__sicom__stomp_server = "comet1.utopia.sicomasp.com";
// $__sicom__stomp_port = 12000;
// $__sicom__comet_port = 80; // We get set to 443 for SSL, don't worry

/* OldSOS writeback */
$__sicom__writeback_alert_email_from = ''; // E.g: utopia.alerts@sicom.com
$__sicom__writeback_alert_email_from_name = ''; // E.g: Utopia Webserver
$__sicom__writeback_alert_email_to = ''; // E.g: writebackalerts@sicom.com
$__sicom__writeback_alert_email_to_name = ''; // E.g: Utopia Group
$__sicom__writeback_server_host = ''; // E.g: 192.168.56.101
$__sicom__writeback_server_port = 0; // E.g: 7271
// Seconds to wait for fsocketopen() to connect.
$__sicom__writeback_server_timeout = 5; // E.g: 5
$__sicom__adoproxy_queue = ''; // For STOMP based writeback. E.g: adoproxy_demo

/* Mail sending */
// General sender of Utopia's emails E. g: cronscripts
$__sicom__general_email_from = ''; // E.g: utopiadev@sicom.com
$__sicom__general_email_from_name = ''; // E.g: Utopia Webserver
// General recipient of Utopia's emails E. g: cronscripts
$__sicom__general_email_to = ''; // E.g: utopiadev@sicom.com
$__sicom__general_email_to_name = ''; // E.g: Utopia Group
// SMTP server
$__sicom__smtp_server = ''; // E.g: mail.sicom.com
 // Append this to every e-mail that the system sends.
$__sicom__email_footer = ''; // E.g: TEST! TEST! TEST! THIS EMAIL WAS SENT FROM A DEVELOPER\'S WORKSTATION. TEST! TEST! TEST!
// Overwrite TO and CC addresses in all outgoing e-mails that the system sends. Add your e-mail address on dev. machines. Leave it empty on a production server.
$__sicom__email_address = ''; // E.g: username@sicom.com

/* Avalara */
$__sicom__avatax_production_mode = false;
// Servlet URL
$__sicom__avalara_servlet_url = '';

/* DMB Portal, webservice password */
 // We pull various informations from DMB Portal via simple HTTP requests.
$__sicom__dmb_portal_webservice_password = ''; // E. g: SecretPa@ssW


/* Daily Support User Password stuff */

/**
 * @var int Port that the password server listens to. Needed for user authentication in certain apps.
 */
$__sicom__pamxpassd_port = 106; // E.g. 106

/**
 * @var string Host that the password server listens on. Needed for user authentication in certain apps.
 */
$__sicom__pamxpassd_host = ''; // E.g. localhost

/**
 * @var string Location of makepass.
 */
$__sicom__makepass_location = ''; // E.g. 192.168.5.6/cgi-bin

/**
 * @var string Filename of the makepass key file that is used for generating daily tech passwords.
 */
$__sicom__makepass_key_file = ''; // E.g. /etc/sicom/makepass_mail_server/makepass.key

/**
 * @var string Filename of the makepass pem file that is used for generating daily tech passwords.
 */
$__sicom__makepass_pem_file = ''; // E.g. /etc/sicom/makepass_mail_server/makepass.pem

/* Ceden */
// Alert user if Ceden did not sent a ping after that time
$__sicom__eden_ping_interval = 30000; // [ms] E. g: 30000

/* Other STOMP settings */
// The name of the message queue that Utopia subscribed to
$__sicom__client_queue_name = ''; // E. g: support_queue2
// The name of the message queue that Ceden subscribed to
$__sicom__server_queue_name = ''; // E. g: ceden
// The name of the message queue to use for general Utopia related purpose
$__sicom__general_queue_name = ''; // E. g: utopia


/* Support Portal (cluffportal) */
$__sicom__support_portal_ip = ''; // IP address of the Support Portal as Utopia sees. We allow access only from that address. E.g: 192.168.5.2