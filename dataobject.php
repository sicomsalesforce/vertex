<?php
/******************************************************************************
 *
 * COPYRIGHT (C) 2009 SICOM Systems Inc
 * 4140 Skyron Drive
 * Doylestown, PA 18902
 * 215-489-2500, FAX: 215-489-2769
 *
 * $Id: dataobject.php,v 1.45 2010/10/25 21:22:12 kedupuganti Exp $
 *
 ******************************************************************************/

/**
 * @file dataobject.php
 * @brief A light weight data object primarily to isolate DB functions from rest of application.
 *
**/
require_once('database_connector.php');
require_once('logger.php');

//2 convenience functions
/** encode_password.
 */
function encode_password($pwd) {
	return md5('there is a wocket in my pocket'.$pwd);
}

/** clean_exit.
 */
function clean_exit() {
	sicom_database_close();
	exit;
}

/** @class DOFactory
 * @brief Create a data object.
 */
// TODO -  No, really, why do we need this? -ajv-
class DOFactory {
	//Call statically ($dob=DOFactory::create('tablename')) to create an instance of a specific DataObject

	public function __construct() {
	}
	
	function &create($table) 	{
		$dob = new DataObject($table);
		return $dob;
	}

	function destroy(&$dob) {
		if (isset($dob))
			$dob->destroy();
		unset($dob);
	}
	
	public function __destruct() {
	}
	
}	// *** End Class: DOFactory ***


// TODO - Why isn't this stuff in a timing library? -ajv-
function getmicrotime() {
	list($usec, $sec) = explode(' ', microtime());
	return ((float)$usec + (float)$sec);
}	// *** End Function: getmicrotime() ***

/**
 * @class KeyFieldFinder
 * @brief Key field finder
 */
class KeyFieldFinder {
	/************************
	 *** Member Variables ***/

	var $_keys;

	/*** End Member Variables ***
	 ****************************/

	function KeyFieldFinder() {
		$this->_keys = array();
		$this->_keys['plu']			= 'plunum';
		$this->_keys['inventory']	= 'inventory_num';
		$this->_keys['recipes']		= 'recipe_uid';
		$this->_keys['recipe_ingredients'] = 'recipe_ingredients_uid';
		$this->_keys['ckey']		= 'ckey_uid';
	}	// *** End Constructor ***

	/*String*/ function getPosKeyValue(&$dob) {
		$result = 'goofy';	// *** Return Value ***
		$tablename = $dob->getTableName();
		if (array_key_exists($tablename, $this->_keys)) {
			$fld = $this->_keys[$tablename];
			$result = $dob->getValue($fld);
		} else {
			log_error("No [$tablename] in KeyFieldFinder");
		}
		//log_debug("getting PosKeyValue for [$fld], value is $result");
		return $result;
	}	// *** End Function: getPosKeyValue(&$dob) ***
}	// *** End Class: KeyFieldFinder ***

/**
 * @class DataObject
*** Light weight data object primarily to isolate DB functions from rest of app
**/
class DataObject {
	var $_table;
	var $_leftjoins = Array();
	var $_innerjoins = Array();
	var $_result;
	var $_cur_row;
	var $_row;
	var $_numrows;			///< The number of rows affected by the latest query executed.
	var $_query;			///< The stored query for the object.
	var $_select;
	var $_where;
	var $_having;
	var $_orderby;
	var $_groupby;
	var $_limit;
	var $_nav_result;
	var $_fieldnames;
	var $_fieldsizes;
	var $_fieldtypes;
	var $_fieldindices;
	var $_debug;
	var $_setValues;
	var $_lastid;
	var $_changed;# = false;
	var $_uid_format; 	///< Like 'field_uid=', so this+_uid = where phrase
	var $_uid;  		///< Uid of current row
	var $_concept_uid;# = 0; ///< So Autoupdate knows which concept when it makes the group
	var $_db_error_string;# = '';
	var $_db_error_errno;# = 0;
	var $m_tableIndeces;# = null;
	var $_last_query_run;

	//function DataObject($table)
	function __construct($table) {
		$this->_select			= '*';
		$this->_table			= $table;
		$this->_row				= -1;
		$this->_numrows			= 0;
		$this->_nav_result		= false;
		$this->_setValues		= array();
		$this->_changed			= false;
		$this->_concept_uid		= 0;
		$this->_db_error_string	= '';
		$this->_db_error_errno	= 0;
		$this->m_tableIndeces	= null;
		log_other(CLOG_DO_CONSTRUCT, 'DATA', "Create $table");
	}	// *** End Constructor ***

	// Mutators

	/** Set the concept number (sems only???)
	 * @param concept is an integer concept number.
	 */
	/*void*/ function setConcept($concept) {
		if (isset($concept) && ($concept > 0)) {
			$this->_concept_uid = $concept;
		}
	}	// *** End Function: Void public setConcept($concept) ***

	/** Set the changed flag.
	 * @param value is a boolean flag marking the object instance as changed.
	 * @returns the new status (NOT A VOID).
	 */
	/*void*/ function setchanged($value) {
		return $this->_changed = $value;
		}

	/** Set the select phrase variable _select.
	 */
	/*void*/ function setSelect($sel) {
		$this->_select = $sel;
	}

	/*void*/ function setOrderBy($orderby) {
		$this->_orderby = $orderby;
	}

	/*void*/ function setGroupBy($groupby) {
		$this->_groupby = $groupby;
	}

	/*void*/ function setWhereUID($wuidquery) {
		$this->_where[0] = $wuidquery;
	}

	/*void*/ function setLimit($limit) {
		$this->_limit = $limit;
	}

	/** Store the query string in the data object as $_query.
	 *  Does not perform the query.
	 * @param q is the SQL query to store for later execution
	 */
	/*void*/ function setQuery($q) {
		//log_debug('query set to ['.$q.']');
		$this->_query = $q;
	}

	/*void*/ function setRowNumber($srow) {
		if (!isset($this->_result) || ($srow < 0)) {	// *** before query executed ***
			$this->_row = $srow;
		} else {
			$this->_loadRow($srow);
		}
	}	// *** End Function: void public setRowNumber(int) ***

	function setTableName($p_table) {
		$this->_table = $p_table;
	}

	/*** End Mutators ***********************/

	// Accessors

	/** Return the SQL query.
	 */
	/*String*/ function getQuery() {
		return $this->_generateSelectQuery();
	}

	function getConcept() {
		return $this->_concept_uid;
	}

	//These are STATIC functions to return DB formatted Date and Time strings
	// that is appropriate to the database being used.
	function getDBDate($utime) {
		return strftime('%Y-%m-%d', $utime);
	}

	function getDBTime($utime) {
		return strftime('%H:%M:%S', $utime);
	}

	/*boolean*/ function isgood() {
		return $this->_nav_result;
	}

	/** Returns the boolean changed flag.
	 */
	function ischanged() {
		return $this->_changed;
	}

	function getRowAsArray() {
		return $this->_cur_row;
	}

	/*int*/ function getSelectFieldCount() {
		return ($this->_result ? mysql_num_fields($this->_result) : 0);
	}

	/*String*/ function getSelectFieldName($idx) {
		return ($this->_result ? mysql_field_name($this->_result, $idx) : '');
	}

	/** Returns the number of rows affected by the stored query.
	 * will execute the select as a side result.
	 */
	/*int*/ function getNumrows() {
		$this->executeSelect(); //Can't do this unless the query has been run
		return $this->_numrows;
	}	// *** End Function: int public getNumrows() ***

	/*int*/ function getNumFields() {
		return ($this->_result ? mysql_num_fields($this->_result) : 0);
	}

	/*String*/ function getFieldName($idx) {
		return ($this->_result ? mysql_field_name($this->_result, $idx) : '');
	}

	/*int*/ function getRowNumber() {
		return $this->_row;
	}

	function getValueIndexed($idx) {
		return $this->_cur_row[$idx];
	}

	/*String*/ function getTableName() {
		return $this->_table;
	}

	/*int*/ function getLastID() {
		return $this->_lastid;
	}

	function getResult() {
		return $this->_result;
	}

/** Fetch the next row from the query and return in the _result numeric array.
 */
	function quickFetchRow() {
		return mysql_fetch_row($this->_result);
	}
	/*** End Accessors ************************/

	/*String*/ function _generateAndOrSection($values, $text) {
		$q = '';
		if (isset($values)) {
			$q = ' '.$text;
			foreach ($values as $key => $val) {
				if ($key != 0) {
					$q .= ' and';
				}
				$q .= ' ('.$val.')';
			}
		}
		return $q;
	}	// *** End Function: String private _generateAndOrSection(String[], String) ***

/** Add Where to the query string.
 */
	function _generateWhereSection() {
		return $this->_generateAndOrSection($this->_where, 'where');
	}

	function _generateHavingSection() {
		return $this->_generateAndOrSection($this->_having, 'having');
	}

	/*String*/ function _generateLimit() {
		return (isset($this->_limit) ? ' limit '.$this->_limit : '');
	}

	function innerJoin($table,$on_clause) {
		$this->_innerjoins[$table] = $on_clause;
	}

	function leftJoin($table,$on_clause) {
		$this->_leftjoins[$table] = $on_clause;
	}

	function _generateJoinSection() {
		$joins = '';

		foreach ($this->_leftjoins as $jtable => $clause) {
			$joins .= " LEFT JOIN $jtable ON $clause";
		}

		foreach ($this->_innerjoins as $jtable => $clause) {
			$joins .= " JOIN $jtable ON $clause";
		}

		return $joins;
	}

	/*String*/ function _generateSelectQuery()
	{
		if ($this->_query)
		{
			//log_debug("Using SET QUERY");
			$q = $this->_query;	// *** Return Value ***
		} else {
			//log_debug("where[0] is ".$this->_where[0]);
			$q = 'SELECT '.$this->_select.' FROM '.$this->_table;	// *** Return Value ***
			$q .= $this->_generateJoinSection();
			$q .= $this->_generateWhereSection();
			$t = $this->_groupby;
			if (isset($t)) {
				$q .= ' GROUP BY '.$t;
			}
			$q .= $this->_generateHavingSection();
			$t = $this->_orderby;
			if (isset($t)) {
				$q .= ' ORDER BY '.$t;
			}
			$q .= $this->_generateLimit();
		}
		return $q;
	}	// *** End Function: String private _generateSelectQuery() ***

	/*String*/ function _generateDeleteQuery() {
		$table = $this->getUpdateableTableName();
		$q = "DELETE FROM $table ";	// *** Return Value ***
		$q .= $this->_generateWhereSection();
		$q .= $this->_generateHavingSection();
		//TODO: Does limit work for delete??? Chet.
		// *** Yes Limit Works For Delete, What About It ? -ajv- ***
		return $q.$this->_generateLimit();
	}	// *** End Function: String private _generateDeleteQuery() ***

	/*String*/ function _generateUpdateQuery() {
		$table = $this->getUpdateableTableName();
		$q = 'UPDATE '.$table.' SET ';	// *** Return Value ***
		foreach ($this->_setValues as $key => $val) {
			$q .= $key.'='.$val.',';
		}
		$q = rtrim($q, ',');
		$q .= $this->_generateWhereSection();
		$q .= $this->_generateHavingSection();
		return $q;
	}	// *** End Function: String private _generateUpdateQuery() ***

	/*String*/ function _generateInsertQuery() {
		$table = $this->getUpdateableTableName();
		$q = 'INSERT INTO '.$table.'(';	// *** Return Value ***
		$v = '';
		foreach ($this->_setValues as $key => $val) {
			$q .= $key.',';
			$v .= $val.',';
		}
		$q = rtrim($q, ',');
		$v = rtrim($v, ',');
		$q .= ") VALUES($v)";
		return $q;
	}	// *** End Function: String private generateInsertQuery() ***

	function whereAddObject(&$where)
	{ $this->_where[] = $where; }

	function whereAdd($where, $make_or = false) {
		//TODO: implement logic for or'ed whereAdd
		$this->_where[] = $where;
	}

	function havingAdd($having, $make_or = false) {
		//TODO: implement or logic
		$this->_having[] = $having;
	}

	function rewind() {
		mysql_data_seek($this->_result, 0);
	}

	/** Loads a row into memory.
	 */
	/*void*/ function _loadRow($rownum, $p_mode = MYSQL_BOTH) {
		if (($rownum >= 0) && ($rownum < $this->_numrows)) {
			mysql_data_seek($this->_result, $rownum);
			$this->_cur_row = mysql_fetch_array($this->_result, $p_mode);
			$this->_row = $rownum;
			//log_debug("Loaded row: $rownum");
			return $this->_nav_result = true;
		} else {
			return $this->_nav_result = false;
		}
	}	// *** End Function: boolean private _loadRow(int) ***

	/// Add database appropriate escapes to strings
	//Use this one like: DataObject::escapeString($str);
	static public function escapeString($str) {
		$db = SicomDatabase::getSicomDatabase();
		return mysql_real_escape_string($str, $db->getConnection());
	}

	/// Save pre-edit values so that we can say what the item is - before any changes.

	/*boolean*/ function delete($force=false) {
		$result = false;
		if (is_null($this->_result)) {
			$db = SicomDatabase::getSicomDatabase();
			$q = $this->_generateDeleteQuery();
			if (is_null($this->_where) && !$force) {
				log_error('Attempt to DELETE without a where clause in '.$this->getUpdateableTableName());
			} else {
				if (!($result = mysql_query($q, $db->getConnection()))) {
					$this->_db_error_errno	= mysql_errno();
					$this->_db_error_string	= mysql_error();
				}
				log_other(CLOG_DO_DELETE, 'DATA', "DELETE result is [$result] for query[$q]");
				$result = ($result >= 1);
			}
		}
		return $result;
	}	// *** End Function: boolean public delete(boolean = false) ***

	/*boolean*/ function update() {
		if ($this->_changed) {
			$q = $this->_generateUpdateQuery();
			$db = SicomDatabase::getSicomDatabase();
			if (!($result = mysql_query($q, $db->getConnection()))) {
				$this->_db_error_errno	= mysql_errno();
				$this->_db_error_string	= mysql_error();
			}
			log_other(CLOG_DO_UPDATE, 'DATA', "UPDATE result is [$result] for query [$q]");
			return ($result == 1);
		} else {
			return false;
		}
	}	// *** End Function: boolean public update() ***

	/*boolean*/ function insert() {
		if ( $this->_changed ) {
			$q	= (is_null($this->_query) ? $this->_generateInsertQuery() : $this->_query);
			$db	= SicomDatabase::getSicomDatabase();
			$connection = $db->getConnection();

			if (!($result = mysql_query($q, $connection)))
			{
				$this->_db_error_errno	= mysql_errno();
				$this->_db_error_string	= mysql_error();
			}
			if ($result == 1) {
				$this->_lastid = mysql_insert_id();
			} else {
				$this->_lastid = -1;
			}
			log_other(CLOG_DO_INSERT, 'DATA', "INSERT result is [$result], lastid = [".$this->_lastid."] for query[$q]");
			return ($result == 1);
		}
		return false;
	}	// *** End Function: boolean public insert() ***

	function getTableIndex($p_keyNumber) {
		if ($this->m_tableIndeces == null) {
			$key_cnt	= 0;
			$cur_key	= '';
			$dob = DOFactory::create('index');
			$dob->setQuery('SHOW INDEX FROM ' . $this->getUpdateableTableName());
			while ($dob->fetch()) {
				$key = $dob->getValue('Key_name');
				$col = $dob->getValue('Column_name');
				if ($cur_key != $key) {
					++$key_cnt;
					$cur_key = $key;
				}
				if ($key_cnt == $p_keyNumber) {
					return $col;	// *** ??? -ajv-- ***
					$this->_fields_in_error[$col] = 1;
					$this->_field_key_list[$key_cnt][$col] = 1;
				}
			}
		} else {
			return $this->m_tableIndeces[$p_keyNumber];
		}
	}   // *** End Function: getTableIndex(int) ***

/** Returns the last error string generated by the database.
 */
	function get_db_error_string() {
		return $this->_db_error_string;
	}

/** Returns the last error number generated by the database.
 */
	function get_db_error_errno() {
		return $this->_db_error_errno;
	}

	/** Find the row with the indicated uid.
	@param col is the column NUMBER to match it against, default is 0
	* */
	/*boolean*/ function findUID($uid = -1, $col = 0) {
		$result = false;
		$this->reset();

		if ($uid < 0) {
			$uid = $this->_lastid;
		}
		while ($this->fetch()) {
			if ($this->getValueIndexed($col) == $uid) {
				$result = true;
				break;
			}
		}
		if (!$result) {
			log_error('Cannot find row with UID='.$uid.' in '.$this->getTableName());
			$this->first(); //Default to first row.
		}
		return $result;
	}	// *** End Function: boolean public findUID(int = -1, int = 0) ***

	/*boolean*/ function select($q = NULL) {
		if (!isset($this->_result) || is_null($this->_result)) {
			$timest = getmicrotime();
			$db = SicomDatabase::getSicomDatabase();
			if (!$q) {
				$q = $this->_generateSelectQuery();
			}
			$this->_last_query_run = $q;
			if (!($this->_result = mysql_query($q, $db->getConnection()))) {
				$this->_db_error_errno	= mysql_errno();
				$this->_db_error_string = mysql_error();
				//output useful information if query fails
				if ($this->_db_error_errno!=0)
					log_error("in dataobject.php: function select() failed; mysql_error: ($this->_db_error_errno) '$this->_db_error_string'\nquery: '$q'\n");
			}
			if (!is_resource($this->_result)) {
				log_fatal("Query FAILED [$q]\n $this->_db_error_string");
				return false;
			} else {
				$this->_numrows = mysql_num_rows($this->_result);
				log_other(CLOG_DO_QUERY, 'DATA', 'numrows: ['.$this->_numrows.'], time(secs): ['.sprintf('%8.6f', getmicrotime() - $timest)."], query: [$q]");
				return true;
			}
		} else {
			//log_debug("DataObject Full - Query FAILED [$q]");
			return false;
		}
	}	// *** End Function: boolean public select(String) ***

	/** Execute the SQL select string.
	 * Loads the selected row into data object memory.
	 * @param q is the select string.
	 * @param p_mode is the mysql data fetch mode.
	 * @returns the success of the select and load row.
	 */
	/*boolean*/ function executeSelect($q = NULL, $p_mode = MYSQL_BOTH) {
		return ($this->select($q) && $this->_loadRow(max(0, $this->_row), $p_mode));
	}

	function reSelect() {
		$this->freeResult();
		return $this->executeSelect();
	}

	/** Fetch gets the current row and then increments the cur_row pointer.
	 * Works like Pear::DB_DataObject fetch function.
	 * @param p_mode is the row storage type - defaults to both numeric and associative
	 * @returns the success of loading a data row - true means row exists and has been loaded.
	 */
	/*boolean*/ function fetch($p_mode = MYSQL_BOTH) {
		if ($this->_row == -1) {
			return ($this->executeSelect('',$p_mode) || $this->_loadRow(0, $p_mode)); //reload row 0
		} else {
			return $this->_loadRow($this->_row + 1, $p_mode);
		}
	}	// *** End Function: boolean public fetch() ***

	/** Resets so that the next fetch will start at the beginning to rescan through the dataset.
	 */
	function reset() {
		$this->_row = -1;
	}

	/****************************
	 *** Navigation Functions ***/

	function first() {
		$this->executeSelect();
		return $this->_loadRow(0);
	}

	function next() {
		$this->executeSelect();
		$t = $this->_row;
		$result = $this->_loadRow($this->_row + 1);
		return $result;
	}

	function previous() {
		$this->executeSelect();
		return $this->_loadRow($this->_row - 1);
	}

	function last() {
		$this->executeSelect();
		return $this->_loadRow($this->_numrows - 1);
	}

	/*** End Navigation Functions ***
	 ********************************/

	function newRow() {
		$this->_row = -1;
	}

	function setValueQuoted($fldname, $value) {
		$this->setValue($fldname, '"'.self::escapeString($value).'"', true);
	}

	/*boolean*/ function setValue($fldname, $value = 'WRONG9876', $dontquote = false) {
		if (is_numeric($value) && $value == 0 )
			$value = '0';
		if ($value == 'WRONG9876') {
			$tn = $this->getUpdateableTableName();
			log_error("Value for field [$fldname] in DataObject [$tn] is NOT SET");
		} else {
			$this->_autoSetFieldInfo();
			if (!array_key_exists($fldname, $this->_fieldindices)) {
				log_error("Field:[$fldname] in table:[".$this->getUpdateableTableName()."] does not exist");
				return false;
			} else {
				if (!$dontquote) {
					$value = '"'.self::escapeString($value).'"';
				}
				$this->_setValues[$fldname] = $value;
				$this->_changed = true;
			}
		}
		return true;
	}	// *** End Function: boolean public setValue(String, String = 'WRONG9876', boolean = false) ***

	/*boolean*/ function setValueIndexed($p_index, $p_value) {
		$result = false;	// *** Return Value ***
		$this->_autoSetFieldInfo();
		if (($p_index >= 0) && ($p_index < count($this->_fieldnames))) {
			$t = $this->_fieldnames[$p_index];
			if (isset($t)) {
				$this->_setValues[$t] = $p_value;
				$result = true;
			}
		}
		if (!$result) {
			log_error("Field index:[$p_index] in table:[".$this->getUpdateableTableName()."] does not exist");
		}
		return $result;
	}	// *** End Function: boolean public setValueIndexed(int, String) ***

	/*void*/ function setFieldIndexByName() {
		$this->executeSelect(); //Can't do this unless the query has been run
		for ($i = $this->getNumFields() - 1; $i >= 0; --$i) {
			$this->_field_index_by_name[ mysql_field_name($this->_result, $i) ] = $i;
		}
		$this->reset();
	}	// *** End Function: void public setFieldIndexByName() ***

	function getFieldIndexByName($name) {
		if (isset($this->_field_index_by_name[$name])) {
			return $this->_field_index_by_name[$name];
		} else {
			print sprintf(_("Field Name: %s, Does not exist\n"),$name);
			exit;
		}
	}	// *** End Function: getFieldIndexByName(String) ***

	/// param $silent prevents the error message if the field is not there
	function getValue($fldname, $silent = false) {
		if (is_array($this->_cur_row) && array_key_exists($fldname, $this->_cur_row)) {
			$result = $this->_cur_row[$fldname];
		} else if (!$silent) {
			$result = '';
			log_error("Fieldname [$fldname] does not exist in table [".$this->getTableName().']');
		} else {
			$result = null;
		}
		return $result;
	}	// *** End Function: getValue(String, boolean = false) ***

	function insertUpdate($query) {
		$db = SicomDatabase::getSicomDatabase();
		$connection = $db->getConnection();

		if (!($set_result = mysql_query($query, $connection)))
		{
			$this->_db_error_errno	= mysql_errno();
			$this->_db_error_string	= mysql_error();
			//output useful information if query fails
			if ($this->_db_error_errno!=0) {
				log_error("in dataobject.php: function insertUpdate() failed; mysql_error: ($this->_db_error_errno) '$this->_db_error_string'\nquery: '$query'\n");
			}
		}
		log_other(CLOG_DO_UPDATE, 'DATA', "insertUpdate result is [$set_result] for query[$query]");
		//added 12/19/2006 rm
		if ($set_result == 1) {
			$this->_lastid = mysql_insert_id();
		} else {
			$this->_lastid = -1;
		}
		//TODO: Evaulate how safe this is
		if ($this->_row == -1) {
			$this->_row = mysql_insert_id() - 1;
			//$this->_lastid = mysql_insert_id();
		}
		return $set_result;
	}	// *** End Function: insertUpdate(String) ***

	/// Returns the FIRST tablename in the _table field.
	/*String*/ function getUpdateableTableName() {
		$x = explode(',', $this->_table);
		return $x[0];
	}	// *** End Function: String public getUpdateableTableName() ***

	/// sets the arrays names, sizes and types according to the column defs
	/*void*/ function getFieldInfo(&$names, &$sizes, &$types, &$indexs) {
		$db = SicomDatabase::getSicomDatabase();
		$conn = $db->getConnection();
		if (isset($conn)) {
			$table = $this->getUpdateableTableName();
			$fields = mysql_list_fields($db->getDatabase(), $table, $conn);
			if (is_resource($fields)) {
				$columns = mysql_num_fields($fields);
				for ($i = 0; $i < $columns; ++$i) {
					$names[] = mysql_field_name($fields, $i);
					$sizes[] = mysql_field_len($fields, $i);
					$types[] = mysql_field_type($fields, $i);
					$indexs[$names[$i]] = $i; //Make a hashtable of name to index
				}
				mysql_free_result($fields);
			} else {
				log_fatal("Unable to get fields for table [$table]");
			}
		}
	}	// *** End Function: void public getFieldInfo(&String[], &int[], &String[], &int[]) ***

	/*void*/ function _autoSetFieldInfo() {
		if (is_null($this->_fieldnames)) {
			$tn = array();
			$ts = array();
			$tt = array();
			$ti = array();
			$this->getFieldInfo($tn, $ts, $tt, $ti);
			$this->_fieldnames = $tn;
			$this->_fieldsizes = $ts;
			$this->_fieldtypes = $tt;
			$this->_fieldindices = $ti;
		}
	}	// *** End Function: void private _autoSetFieldInfo() ***

	/*void*/ function freeResult() {
		if (isset($this->_result) && ($this->_result != 0)) {
			//log_debug("freeResult for table: ".$this->getTableName().": _result is ".$this->_result);
			mysql_free_result($this->_result);
			unset($this->_result);
		} else {
			// This is ok sometimes because in transaction Data Objects there will no results (Like a day with no petty cash)
			//log_error('DataObject::freeResult for table ['.$this->getTableName().'] called with _result=['.$this->_result.']');
		}
	}	// *** End Function: void public freeResult() ***

	/*void*/ function resetQuery($keeprownumber = false) {
		//log_debug('resetQuery for table: '.$this->getTableName(). ', query '.$this->getQuery());
		$this->freeResult();
		if (!$keeprownumber) {
			$this->_row = -1;
		}
		$this->_numrows = 0;
		$this->_cur_row = null;
	}	// *** End Function: boolean public resetQuery(boolean = false) ***

	function reQuery($keeprownumber = false) {
		$this->resetQuery($keeprownumber);
		$this->executeSelect();
	}

	function destroy() {	//Call when done
		$this->resetQuery();
	}

	function crossDBAutoIncrement($field) {
		return "$field INT NOT NULL AUTO_INCREMENT ";
	}

	function crossDBGetDataTimeSQLKeyWord() {
		return "DATETIME ";
	}

	function crossDBGetTinyIntSQLKeyWord() {
		return "TINYINT ";
	}

	function crossDBGetBiggerTimeSQLKeyWord() {
		return "TIME ";
	}

	function crossDBGetTimestampSQLKeyWord() {
		return "TIMESTAMP(14) ";
	}

	function crossDBGetUniqueKeySQLKeyWord() {
		return "UNIQUE KEY ";
	}

	function crossDBGetAlterTable($table, $column, $type, $default = "") {
		return "ALTER TABLE $table MODIFY COLUMN $column $type $default ";
	}

	function crossDBGetRenameCoulumn($table, $old_name, $new_name, $definiton) {
		return "ALTER TABLE $table change $old_name $new_name $definiton ";
	}

	function crossDBDate_sub($field, $interval) {
		return "DATE_SUB($field, INTERVAL $interval) ";
	}

	function crossDBDate_add($field, $interval) {
		return "DATE_ADD($field, INTERVAL $interval) ";
	}

	function begin() {
		$db = SicomDatabase::getSicomDatabase();
		log_debug("TRANSACTION BEGIN");
		mysql_query("BEGIN", $db->getConnection());
	}

	function commit() {
		$db = SicomDatabase::getSicomDatabase();
		log_debug("TRANSACTION COMMIT");
		mysql_query("COMMIT", $db->getConnection());
	}

	function rollback() {
		$db = SicomDatabase::getSicomDatabase();
		log_debug("TRANSACTION ROLLBACK");
		mysql_query("ROLLBACK", $db->getConnection());
	}
	
	public function __destruct() {
		$this->destroy();
	}
	
}	// *** End Class: DataObject ***
?>
