<?php
/*****************************************************************
* 
* COPYRIGHT (C) 2004-2012 SICOM Systems Inc
* 4434 Progress Meadow Drive
* Doylestown, PA 18902
* 215-489-2500, FAX: 215-489-2769
* 
* $Id$
* 
* (This file is a part of the AvaTax4PHP library, and has only
*  been edited to include SICOM's Avalara credentials)
* 
*****************************************************************/



// ATConfig object is how credentials are set
// Tax or Address Service Objects take an argument
// which is the name of the ATConfig object ('Test' or 'Prod' below)


/* This is a configuration called 'Development'. 
 * Only values different from 'Default' need to be specified.
 * Example:
 *
 * $service = new AddressServiceSoap('Development');
 * $service = new TaxServiceSoap('Development');
 */
new ATConfig('Development', array(
	'url'       => 'https://development.avalara.net',
	'account'   => '1100067825',
	'license'   => '888A548EDF7EF8A8',
	'trace'     => true) // change to false for production
);

/* This is a configuration called 'Production' 
 * Example:
 *
 * $service = new AddressServiceSoap('Production');
 * $service = new TaxServiceSoap('Production');
 */
new ATConfig('Production', array(
	'url'       => 'https://avatax.avalara.net',
	'account'   => '1100014270',
	'license'   => '21A9A2571F4B2993',
	'trace'     => false) // change to false for production
);


?>
