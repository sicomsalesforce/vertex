<?php
/**
 * @copyright (C) 2004-2017 SICOM Systems Inc
 *            4434 Progress Meadow Drive
 *            Doylestown, PA 18902
 *            215-489-2500, FAX: 215-489-2769
 *
 * This file contains a library used to communicate with our tax
 * software, Avalara.  There are three primary classes - AvalaraTest,
 * AvalaraAddress, and AvalaraInvoice.  AvalaraTest is used to check
 * the connection to SICOM's avalara servlet, and optionally, the servlet's
 * connection to Avalara itself.  AvalaraAddress is used to validate
 * an address for future tax calculation, and AvalaraInvoice is used
 * to calculate sales tax for an invoice.
 *
 * The following elements are particularly relevant:
 *
 * :: Servlet Url
 *   Avalara Tax software servlet URL has to be set in the siteunique
 *   variable $__sicom__avalara_servlet_url.
 *
 * :: Account Code
 *    The account code of a customer is a six character code used
 *    to represent a customer in MAS 90, our accounting software
 *    (e.g. TWO001).  Providing the account code to Avalara is
 *    important because some customers are tax exempt, and Avalara
 *    needs to know who it's dealing with.
 *
 * :: Address
 *    The shipto address of a customer is important because it
 *    helps Avalara know which area's tax laws apply to it.  Before
 *    calculating tax for an invoice, you should validate the
 *    address; this converts the address in our system into an
 *    address which Avalara will recognize.
 *
 * :: Prices
 *    SICOM stores its prices as integers in the database.  This
 *    API takes care of converting from SICOM's number format to
 *    Avalara's (and vice-versa), so you don't need to worry about
 *    that detail; just provide the pricing straight out of the db.
 *
 * :: Part Numbers
 *    Avalara recognizes the SICOM part numbers we use in MAS 90.
 *    When providing part numbers, use that format.
 *    (e.g. P1903-DMB1KA)
 *
 * :: Sample Code
 *
 *    // build invoice header
 *    $invoice = new AvalaraInvoice();
 *    $invoice->accountCode ('MYE001'              );
 *    $invoice->address1    ('3000 N. Garfield 280');
 *    $invoice->city        ('Midland'             );
 *    $invoice->region      ('TX'                  );
 *    $invoice->postalCode  ('76904-6812'          );
 *    $invoice->country     ('USA'                 );
 *
 *    // build invoice line items
 *    $invoice->addLineItem(true, 'P1903-DMB1KA',  87500,  4);
 *    $invoice->addLineItem(true, 'P1900-DMBKA',   50000,  1);
 *    $invoice->addLineItem(true, 'P1996-4-DMBKA', 28000,  1);
 *    $invoice->addLineItem(true, 'P1902-DMB1KA',  52500,  4);
 *    $invoice->addLineItem(true, 'P1992-DMB1KA',  5600,   4);
 *    $invoice->addLineItem(true, 'P1912-DMB',     137500, 1);
 *    $invoice->addLineItem(true, 'P1916-DMB',     000,    1);
 *    $invoice->addLineItem(true, 'P1926-DMBKA',   000,    1);
 *    $invoice->addLineItem(true, 'P1927-DMB',     36000,  1);
 *    $invoice->addLineItem(true, '/SHDMB',        30700,  1);
 *
 *    // grab tax
 *    try {
 *        $invoice->validateAddress();
 *        echo $invoice->tax();
 *    } catch(Exception $e) {
 *        log_error($e);
 *    }
 *
 */



//|| Defines
define('MAS_COMPANY',   'SSI'                       ); // the main company in MAS 90
define('SICOM_ADDRESS', '4434 Progress Meadow Drive');
define('SICOM_CITY',    'Doylestown'                );
define('SICOM_STATE',   'PA'                        );
define('SICOM_ZIP',     '18902'                     );
define('SICOM_COUNTRY', 'USA'                       );

// define('SERVLET_URL', 'http://avalara-sems.sicom.com/~utopia/utopia/AvalaraAPIServlet.php');
// -- URL moved to siteunique variable $__sicom__avalara_servlet_url
define('SERVLET_PASSCODE', '4fa7e5b0eaec4');
define('REQUEST_TYPE_TEST',     1); // request types must be integers
define('REQUEST_TYPE_VALIDATE', 2);
define('REQUEST_TYPE_TAX',      3);



//|| Ensure JSON Is Loaded
if (!extension_loaded('json'))
	@dl('json.so');



//|| Class: AvalaraTest
/**
 * This class is used to test the connection to the Avalara servlet.
 */
class AvalaraTest {
	//|| Member Variables ||//
	private $check_avalara_connection = false;
	private $error                    = NULL;
	private $production_mode          = true;
	
	
	
	//|| Constructor ||//
	/**
	 * Constructs the test, setting the production mode to
	 * $__sicom__avatax_production_mode if set.  If not set, it will
	 * default to production mode.
	 */
	public function AvalaraTest() {
		//:: Determine Mode
		global $__sicom__avatax_production_mode;
		if (isset($__sicom__avatax_production_mode) && $__sicom__avatax_production_mode == false)
			$this->production_mode = false;
		else
			$this->production_mode = true;
	}
	
	
	
	//|| Gets / Sets ||//
	/**
	 * Gets/sets whether or not test() will also check the connection
	 * to Avalara itself.
	 * 
	 * This is set to false by default, as every time Avalara is contacted,
	 * it costs SICOM money.  Take this into consideration before setting
	 * this to true.
	 */
	public function checkAvalaraConnection($value = NULL) {
		if ($value !== NULL)
			$this->check_avalara_connection = $value;
		return $this->check_avalara_connection;
	}
	
	/**
	 * Gets the Exception which was created while testing the connection.
	 */
	public function error() {
		return $this->error;
	}
	protected function setError($value) {
		$this->error = $value;
	}
	
	/**
	 * Gets/sets whether or not avalara's production server should be
	 * contacted.
	 * 
	 * A false value denotes the use of their development server.
	 */
	public function productionMode($value = NULL) {
		if($value !== NULL) {
			$this->production_mode = $value;
		}
		return $this->production_mode;
	}
	
	
	
	//|| Public Functions ||//
	/**
	 * Tests the connection to the servlet.
	 * 
	 * @return true if the connection is fine; false otherwise.
	 */
	public function test() {
		//:: Variables
		$post_data = "";
		$header    = NULL;
		$request   = NULL;
		$servlet   = NULL;
		$response  = NULL;
		
		//:: Build POST Data
		$post_data = json_encode(new AvalaraServletRequest(REQUEST_TYPE_TEST, $this->cloneJSON()));
		if (!$post_data) {
			$exception = new AvalaraGeneralException();
			$exception->message("The AvalaraTest object could not be json_encoded.");
			throw $exception;
		}
		
		//:: Build Header
		$header = array(
			'http' => array(
				'method'  => 'POST',
				'header'  => 'Content-type: application/x-www-form-urlencoded',
				'content' => $post_data
			)
		);
		
		//:: Open Socket
		$request = stream_context_create($header);
		$servlet = @fopen(AvalaraInvoice::getServletUrl(), 'r' , false , $request);
		if (!$servlet) {
			$exception = new AvalaraConnectionException();
			$exception->message("SICOM's servlet could not be contacted.");
			$this->setError($exception);
			return false;
		}
		
		//:: Retrieve Data
		$response = stream_get_contents($servlet);
		$response = json_decode($response);
		if (!$response) {
			$exception = new AvalaraConnectionException();
			$exception->message("The Avalara servlet returned missing or corrupt data.");
			$this->setError($exception);
			return false;
		}
		if ($response->errors) {
			$this->setError(JSONInflatableException::getException($response->exception));
			fclose($servlet);
			return false;
		} else {
			$this->decloneJSON($response->request_object);
			fclose($servlet);
			return true;
		}
	}
	
	/**
	 * This function returns a copy of this object which can be
	 * serialized into a JSON string
	 */
	public function cloneJSON() {
		//:: Variables
		$copy = new stdClass();
		
		//:: Copy Object
		foreach ($this as $key => $value)
			$copy->$key = $value;
		
		//:: Return Copy
		return $copy;
	}
	
	/**
	 * This function copies values to this object from a json_decoded
	 * object.
	 */
	public function decloneJSON($copy) {
		//:: Copy Object
		foreach ($copy as $key => $value)
			$this->$key = $value;
	}
}



//|| Class: AvalaraAddress
class AvalaraAddress {
	//|| Member Variables ||//
	//   destination address
	private $address1     = '';
	private $address2     = '';
	private $address3     = '';
	private $city         = '';
	private $region       = ''; // e.g. state
	private $postal_code  = '';
	private $country      = 'USA';

	//   clerical stuff
	//   (dirty flag helps us to know when to validate address)
	private $validated       = false;
	private $production_mode = true;



	//|| Constructor ||//
	/**
	 * Constructs the address, setting the production mode to
	 * $__sicom__avatax_production_mode if set.  If not set, it will
	 * default to production mode.
	 */
	public function AvalaraAddress() {
		//:: Determine Mode
		global $__sicom__avatax_production_mode;
		if (isset($__sicom__avatax_production_mode) && $__sicom__avatax_production_mode == false)
			$this->production_mode = false;
		else
			$this->production_mode = true;
	}



	//|| Gets / Sets ||//

	/**
	 * Gets/sets the first address line.
	 */
	public function address1($value = NULL) {
		if($value !== NULL) {
			$this->address1  = $value;
			$this->validated = false;
		}
		return $this->address1;
	}

	/**
	 * Gets/sets the second address line.
	 */
	public function address2($value = NULL) {
		if($value !== NULL) {
			$this->address2  = $value;
			$this->validated = false;
		}
		return $this->address2;
	}

	/**
	 * Gets/sets the third address line.
	 */
	public function address3($value = NULL) {
		if($value !== NULL) {
			$this->address3  = $value;
			$this->validated = false;
		}
		return $this->address3;
	}

	/**
	 * Gets/sets the city.
	 */
	public function city($value = NULL) {
		if($value !== NULL) {
			$this->city      = $value;
			$this->validated = false;
		}
		return $this->city;
	}

	/**
	 * Gets/sets the region (e.g. state).
	 */
	public function region($value = NULL) {
		if($value !== NULL) {
			$this->region    = $value;
			$this->validated = false;
		}
		return $this->region;
	}

	/**
	 * Gets/sets the zip/postal code.
	 */
	public function postalCode($value = NULL) {
		if($value !== NULL) {
			$this->postal_code = $value;
			$this->validated   = false;
		}
		return $this->postal_code;
	}

	/**
	 * Gets/sets the country.
	 */
	public function country($value = NULL) {
		if($value !== NULL) {
			$this->country   = $value;
			$this->validated = false;
		}
		return $this->country;
	}

	/**
	 * Gets/sets whether or not the address has been validated since the
	 * last change to its members.
	 */
	public function validated($value = NULL) {
		if($value !== NULL) {
			$this->validated = $value;
		}
		return $this->validated;
	}

	/**
	 * Gets/sets whether or not avalara's production server should be
	 * contacted.
	 *
	 * A false value denotes the use of their development server.
	 */
	public function productionMode($value = NULL) {
		if($value !== NULL) {
			$this->production_mode = $value;
		}
		return $this->production_mode;
	}



	//|| Public Functions ||//

	/**
	 * Sends the address stored in this object to Avalara for validation.
	 * Upon receiving a response, it updates the stored address to the
	 * valid address.
	 *
	 * @throws AvalaraAddressException
	 *     thrown if Avalara wasn't able to validate the address.
	 * @throws AvalaraConnectionException
	 *     thrown if there was a problem connecting to the servlet.
	 * @throws AvalaraGeneralException
	 *     thrown if a general exception occurred.
	 */
	public function validate() {
		//:: Variables
		$post_data = "";
		$header    = NULL;
		$request   = NULL;
		$servlet   = NULL;
		$response  = NULL;

		//:: Check If Address Needs To Be Validated
		if ($this->validated())
			return;

		//:: Build POST Data
		$post_data = json_encode(new AvalaraServletRequest(REQUEST_TYPE_VALIDATE, $this->cloneJSON()));
		if (!$post_data) {
			$exception = new AvalaraGeneralException();
			$exception->message("The AvalaraAddress object could not be json_encoded.");
			throw $exception;
		}

		//:: Build Header
		$header = array(
			'http' => array(
				'method'  => 'POST',
				'header'  => 'Content-type: application/x-www-form-urlencoded',
				'content' => $post_data
			)
		);

		//:: Open Socket
		$request = stream_context_create($header);
		$servlet = @fopen(AvalaraInvoice::getServletUrl(), 'r' , false , $request);
		if (!$servlet) {
			$exception = new AvalaraConnectionException();
			$exception->message(_("Could not connect to the Avalara servlet."));
			throw $exception;
		}

		//:: Retrieve Data
		$response = stream_get_contents($servlet);
		$response = json_decode($response);
		if (!$response) {
			$exception = new AvalaraConnectionException();
			$exception->message("The Avalara servlet returned missing or corrupt data.");
			throw $exception;
		}
		if ($response->errors) {
			throw JSONInflatableException::getException($response->exception);
		} else
			$this->decloneJSON($response->request_object);

		//:: Close Socket
		fclose($servlet);
	} // end validate()

	/**
	 * This function returns a copy of this object which can be
	 * serialized into a JSON string
	 */
	public function cloneJSON() {
		//:: Variables
		$copy = new stdClass();

		//:: Copy Object
		foreach ($this as $key => $value)
			$copy->$key = $value;

		//:: Return Copy
		return $copy;
	}

	/**
	 * This function copies values to this object from a json_decoded
	 * object.
	 */
	public function decloneJSON($copy) {
		//:: Copy Object
		foreach ($copy as $key => $value)
			$this->$key = $value;
	}
}




//|| Class: AvalaraInvoice
/**
 * This class holds information pertinent to an Avalara invoice
 */
class AvalaraInvoice {
	protected $address = null;
	/** @var string Avalara servlet URL*/
	private static $servletUrl = null;
	private $account_code = '';
	private $invoice_date = '';
	private $tax = -1;
	private $line_items = array();

	//   clerical stuff
	//   (dirty flags help us to know when to calculate tax && validate address)
	private $modified        = true;
	private $production_mode = true;
	
	
	
	//|| Constructor ||//
	/**
	 * Constructs the invoice, setting the production mode to
	 * $__sicom__avatax_production_mode if set.  If not set, it will
	 * default to production mode.
	 */
	public function AvalaraInvoice() {
		//:: Set Non-Primitives
		$this->invoice_date = time(); // defaults to now
		$this->address      = new AvalaraAddress();
		
		//:: Determine Mode
		global $__sicom__avatax_production_mode;
		if (isset($__sicom__avatax_production_mode) && $__sicom__avatax_production_mode == false)
			$this->production_mode = false;
		else
			$this->production_mode = true;
	}

	/**
	 * @return string
	 */
	public static function getServletUrl() {
		global $__sicom__avalara_servlet_url;

		if (isset($__sicom__avalara_servlet_url) && filter_var($__sicom__avalara_servlet_url, FILTER_VALIDATE_URL)) {
			self::$servletUrl = $__sicom__avalara_servlet_url;
			log_debug(sprintf('Avalara servlet URL: %s', self::$servletUrl));
		} else {
			throw new Exception(
				sprintf("Avalara servlet URL invalid or missing in siteunique, check the [%s] variable.", '$__sicom__avalara_servlet_url')
			);
		}

		return self::$servletUrl;
	}

	//|| Gets / Sets ||//
	/**
	 * Gets/sets the customer's account code.  This is important because
	 * some customers are tax exempt.
	 */
	public function accountCode($value = NULL) {
		if($value !== NULL) {
			$this->account_code = $value;
			$this->modified = true;
		}
		return $this->account_code;
	}
	
	/**
	 * Gets/sets the invoice date.  This is important because it determines
	 * tax based on the tax laws at that time.  If not set, it defaults
	 * to today.
	 * 
	 * @param $value
	 *     A timestamp representing the invoice date.
	 */
	public function invoiceDate($value = NULL) {
		if($value !== NULL) {
			$this->invoice_date = $value;
			$this->modified = true;
		}
		return $this->invoice_date;
	}
	
	/**
	 * Gets/sets the destination's first address line.
	 */
	public function address1($value = NULL) {
		if($value !== NULL) {
			$this->address->address1($value);
			$this->modified = true;
		}
		return $this->address->address1();
	}
	
	/**
	 * Gets/sets the destination's second address line.
	 */
	public function address2($value = NULL) {
		if($value !== NULL) {
			$this->address->address2($value);
			$this->modified = true;
		}
		return $this->address->address2();
	}
	
	/**
	 * Gets/sets the destination's third address line.
	 */
	public function address3($value = NULL) {
		if($value !== NULL) {
			$this->address->address3($value);
			$this->modified = true;
		}
		return $this->address->address3();
	}
	
	/**
	 * Gets/sets the destination's city.
	 */
	public function city($value = NULL) {
		if($value !== NULL) {
			$this->address->city($value);
			$this->modified = true;
		}
		return $this->address->city();
	}
	
	/**
	 * Gets/sets the destination's region (e.g. state).
	 */
	public function region($value = NULL) {
		if($value !== NULL) {
			$this->address->region($value);
			$this->modified = true;
		}
		return $this->address->region();
	}
	
	/**
	 * Gets/sets the destination's zip/postal code.
	 */
	public function postalCode($value = NULL) {
		if($value !== NULL) {
			$this->address->postalCode($value);
			$this->modified = true;
		}
		return $this->address->postalCode();
	}
	
	/**
	 * Gets/sets the destination's country.
	 */
	public function country($value = NULL) {
		if($value !== NULL) {
			$this->address->country($value);
			$this->modified = true;
		}
		return $this->address->country();
	}
	
	/**
	 * Gets the total tax based off of the information stored in $this
	 * object.
	 * 
	 * @throws AvaleraTaxException
	 *     thrown if an error occurs while calculating tax.
	 * @throws SoapFault
	 *     thrown if there was a problem connecting to Avalara.
	 */
	public function tax() {
		try {
			$this->calculateTax();
			return $this->tax;
		} catch (AddressNotValidException $e) {
			throw $e;
		}
	}
	protected function setTax($value) {
		$this->tax = $value;
	}
	
	/**
	 * Gets/sets whether or not the invoice has been modified since the
	 * last tax calculation.
	 */
	public function modified($value = NULL) {
		if($value !== NULL) {
			$this->modified = $value;
		}
		return $this->modified;
	}
	
	/**
	 * Gets/sets whether or not the address has been validated since the
	 * last change to address fields.
	 */
	public function validated($value = NULL) {
		return $this->address->validated($value);
	}
	
	/**
	 * Gets/sets whether or not avalara's production server should be
	 * contacted.
	 * 
	 * A false value denotes the use of their development server.
	 */
	public function productionMode($value = NULL) {
		if($value !== NULL) {
			$this->production_mode = $value;
		}
		return $this->production_mode;
	}
	
	
	
	//|| General Public Functions ||//
	
	/**
	 * Adds a line item to the invoice.
	 * @param $return_object
	 *     (defaults to true) Determines whether or not to return a the
	 *     new AvalaraLineItem object.  If false, the index of the new
	 *     object will be returned instead.
	 * @param $part_number
	 *     (optional) the SICOM part number of the item
	 * @param $price
	 *     (optional) the price of a single unit of this item
	 * @param $quantity
	 *     (optional) The number of units of this item being sold
	 * 
	 * @returns
	 *     The object/index of the new line.
	 */
	public function addLineItem($return_object = true, $part_number = NULL, $price = NULL, $quantity = NULL, $description = NULL) {
		//:: Set Dirty Flag
		$this->modified(true);
		
		//:: Grab New Line's Index
		$index = count($this->line_items);
		
		//:: Create New Line Item
		if      ($description !== NULL)
			$this->line_items[] = new AvalaraLineItem($this, $index, $part_number, $price, $quantity, $description);
		else if ($quantity    !== NULL)
			$this->line_items[] = new AvalaraLineItem($this, $index, $part_number, $price, $quantity);
		else if ($price       !== NULL)
			$this->line_items[] = new AvalaraLineItem($this, $index, $part_number, $price);
		else if ($part_number !== NULL)
			$this->line_items[] = new AvalaraLineItem($this, $index, $part_number);
		else
			$this->line_items[] = new AvalaraLineItem($this, $index);
		
		//:: Return Index/Object Reference
		if ($return_object)
			return $this->line_items[$index];
		else
			return $index;
	}
	
	/**
	 * Returns the MAS90LineItem object at the provided index.
	 */
	public function getLineItem($index) {
		return $this->line_items[$index];
	}
	
	/**
	 * Returns the number of line items associated with this record
	 */
	public function countLineItems() {
		return count($this->line_items);
	}
	
	/**
	 * Sends the address stored in this object to Avalara for validation.
	 * Upon receiving a response, it updates the stored address to the
	 * valid address.
	 * 
	 * @throws AvalaraAddressException
	 *     thrown if Avalara wasn't able to validate the address.
	 * @throws AvalaraConnectionException
	 *     thrown if there was a problem connecting to the servlet.
	 * @throws AvalaraGeneralException
	 *     thrown if a general exception occurred.
	 */
	public function validateAddress() {
		//:: Check If Address Needs To Be Validated
		if ($this->validated())
			return;
		
		//:: Validate
		$this->address->validate();
		
		//:: Update Dirty Flag
		$this->modified(true);
	}
	
	/**
	 * (re)Calculates the tax if necessary.  Calling this function is not
	 * necessary, as a call to tax() will automatically call this function.
	 * 
	 * @throws AvaleraTaxException
	 *     thrown if an error occurs while calculating tax.
	 * @throws SoapFault
	 *     thrown if there was a problem connecting to Avalara.
	 */
	function calculateTax() {
		//:: Variables
		$post_data = "";
		$header    = NULL;
		$request   = NULL;
		$servlet   = NULL;
		$response  = NULL;
		
		//:: Check If Tax Needs To Be Calculated
		if (!$this->modified())
			return;
		
		//:: Build POST Data
		$post_data = json_encode(new AvalaraServletRequest(REQUEST_TYPE_TAX, $this->cloneJSON()));
		if (!$post_data) {
			$exception = new AvalaraGeneralException();
			$exception->message("The AvalaraInvoice object could not be json_encoded.");
			throw $exception;
		}
		
		//:: Build Header
		$header = array(
			'http' => array(
				'method'  => 'POST',
				'header'  => 'Content-type: application/x-www-form-urlencoded',
				'content' => $post_data
			)
		);
		
		//:: Open Socket
		$request = stream_context_create($header);
		$servlet = @fopen(self::getServletUrl(), 'r' , false , $request);
		if (!$servlet) {
			$exception = new AvalaraConnectionException();
			$exception->message("Could not connect to the Avalara servlet.");
			throw $exception;
		}
		
		//:: Retrieve Data
		$response = stream_get_contents($servlet);
		$response = json_decode($response);
		if (!$response) {
			$exception = new AvalaraConnectionException();
			$exception->message("The Avalara servlet returned missing or corrupt data.");
			throw $exception;
		}
		if ($response->errors)
			throw JSONInflatableException::getException($response->exception);
		else
			$this->decloneJSON($response->request_object);
		
		//:: Close Socket
		fclose($servlet);
	} // end calculateTax()
	
	/**
	 * This function returns a copy of this object which can be
	 * serialized into a JSON string
	 */
	public function cloneJSON() {
		//:: Variables
		$copy       = new stdClass();
		$line_items = array();
		
		//:: Copy Object
		foreach ($this as $key => $value) {
			// clone line items
			if ($key == 'line_items') {
				foreach ($value as $index => $line_item)
					$line_items[$index] = $line_item->cloneJSON();
				$copy->$key = $line_items;
			
			// clone address
			} else if ($key == 'address')
				$copy->$key = $this->$key->cloneJSON();
			
			// clone other fields
			else
				$copy->$key = $value;
		}
		
		//:: Return Copy
		return $copy;
	}
	
	/**
	 * This function copies values to this object from a json_decoded
	 * object.
	 */
	public function decloneJSON($copy) {
		//:: Variables
		$line_items = NULL;
		
		//:: Copy Object
		foreach ($copy as $key => $value) {
			// declone line items
			if ($key == 'line_items') {
				$line_items = &$this->$key;
				foreach ($value as $index => $line_item) {
					// if line item already exists, replace values
					if (isset($line_items[$index]))
						$line_items[$index]->decloneJSON($line_item);
					
					// if line item doesn't already exist, create new line item
					else {
						$line_items[$index] = new AvalaraLineItem($this, $index);
						$line_items[$index]->decloneJSON($line_item);
					}
				}
			
			// declone address
			} else if ($key == 'address')
				$this->$key->decloneJSON($value);
			
			// declone other fields
			else
				$this->$key = $value;
		}
	}
} // end AvalaraInvoice



//|| Class: AvalaraLineItem
/**
 * This class holds data pertinent to an Avalara invoice line item
 */
class AvalaraLineItem {
	//|| Member Variables ||//
	private $parent;
	private $part_number;
	private $description;
	private $price;
	private $quantity;
	private $modified;
	private $tax;
	
	
	
	//|| Constructor ||//
	/**
	 * Constructs the line item.
	 * 
	 * @param $parent
	 *     The AvalaraInvoice object which contains this line item.
	 * @param $part_number
	 *     The MAS 90 part number; defaults to ''.
	 * @param $price
	 *     The price of a single unit of this item; defaults to 0.
	 * @param $quantity
	 *     The number of units of this item being sold; defaults to 1.
	 * @param $description
	 *     A description of the part number
	 */
	public function AvalaraLineItem($parent, $index, $part_number = '', $price = 0, $quantity = 1, $description = '') {
		$this->parent      = $parent;
		$this->index       = $index;
		$this->modified    = true;
		$this->part_number = $part_number;
		$this->price       = $price;
		$this->quantity    = $quantity;
		$this->description = $description;
		$this->tax         = -1;
	}
	
	
	
	//|| Gets / Sets ||//
	/**
	 * Gets the line item's parent AvalaraInvoice object.
	 */
	public function parent() {
		return $this->parent;
	}
	
	/**
	 * Gets the line item's parent AvalaraInvoice object.
	 */
	public function index() {
		return $this->index;
	}
	
	/**
	 * Gets/sets whether or not this line item has been modified since the
	 * last tax calculation
	 */
	public function modified($value = NULL) {
		if($value !== NULL) {
			$this->modified = $value;
			if($value == true) // clarity
				$this->parent->modified(true);
		}
		return $this->modified;
	}
	
	/**
	 * Gets/sets the line item's part number.
	 */
	public function partNumber($value = NULL) {
		if($value !== NULL) {
			$this->part_number = $value;
			$this->modified(true);
		}
		return $this->part_number;
	}
	
	/**
	 * Gets/sets the part number's description.
	 */
	public function description($value = NULL) {
		if($value !== NULL) {
			$this->description = $value;
			$this->modified(true);
		}
		return $this->description;
	}
	
	/**
	 * Gets/sets the line item's price.
	 */
	public function price($value = NULL) {
		if($value !== NULL) {
			$this->price = $value;
			$this->modified(true);
		}
		return $this->price;
	}
	
	/**
	 * Gets/sets the line item's quantity.
	 */
	public function quantity($value = NULL) {
		if($value !== NULL) {
			$this->quantity = $value;
			$this->modified(true);
		}
		return $this->quantity;
	}
	
	/**
	 * Gets the line item's tax based on the information stored in this
	 * object, and its parent invoice.
	 * 
	 * @throws AvaleraTaxException
	 *     thrown if an error occurs while calculating tax.
	 * @throws SoapFault
	 *     thrown if there was a problem connecting to Avalara.
	 */
	public function tax() {
		$this->parent->calculateTax();
		return $this->tax;
	}
	
	
	
	//|| Public Functions ||//
	/**
	 * This function updates this line item with fresh tax information.
	 * 
	 * It is only meant to be called by classes in this API; it isn't
	 * intended for use by the application layer.
	 */
	public function updateTax($tax_object) {
		$this->tax = intval(round((((float) $tax_object->getTax()) * 100), 2));
	}
	
	/**
	 * This function returns a copy of this object which can be
	 * serialized into a JSON string
	 */
	public function cloneJSON() {
		//:: Create Copy
		$copy = new stdClass();
		
		//:: Copy Object
		foreach ($this as $key => $value) {
			if ($key != 'parent')
				$copy->$key = $value;
		}
		
		//:: Return Copy
		return $copy;
	}
	
	/**
	 * This function copies values to this object from a json_decoded
	 * object.
	 */
	public function decloneJSON($copy) {
		//:: Copy From Object
		foreach ($copy as $key => $value) {
			if ($key != 'parent')
				$this->$key = $value;
		}
	}
} // end AvalaraLineItem



//|| Class: JSONInflatableException
/**
 * Exception which contains a utility function to help with the
 * inflation of exceptions over a json connection.
 * 
 * All exception members should be declared public, making it json-
 * encodable.  Then on the receiving end, json_decode the response,
 * and instantiate the exception with getException().  Because of
 * the generic factory creation of getException, classes which
 * extend JSONInflatableException should have a zero-argument
 * constructor.
 */
abstract class JSONInflatableException extends Exception {
	//|| Data Members ||//
	public $exception_type = 'JSONInflatableException';
	
	
	
	//|| Gets / Sets ||//
	/**
	 * Gets/sets the class name of the exception.  This should
	 * be set in the extending class' constructor.
	 */
	public function exceptionType($value = NULL) {
		if ($value !== NULL)
			$this->exception_type = $value;
		return $this->exception_type;
	}
	
	
	
	//|| Public Methods ||//
	/**
	 * Inflates the exception from the provided json-decoded object
	 */
	function inflateJSON($copy) {
		//:: Copy From Object
		foreach ($copy as $key => $value)
			$this->$key = $value;
	}
	
	/**
	 * Static method which returns an exception based on the provided
	 * json-decoded exception data.
	 */
	public static function getException($data) {
		//:: Create Exception
		$type      = $data->exception_type;
		$exception = new $type();
		
		//:: Inflate Exception
		$exception->inflateJSON($data);
		
		//:: Return Exception
		return $exception;
	}
}



//|| Class: AvalaraGeneralException
/**
 * Thrown when a general error has occurred (maps to Exception,
 * except that this exception can be deflated and inflated on
 * both sides of a JSON transaction).
 */
class AvalaraGeneralException extends JSONInflatableException {
	//|| Data Members ||//
	public $message;
	
	
	
	//|| Gets / Sets ||//
	/**
	 * Gets/sets the exception's message
	 */
	public function message($value = NULL) {
		if ($value !== NULL)
			$this->message = $value;
		return $this->message;
	}
	
	
	
	//|| Constructor
	/**
	 * Builds the exception.
	 */
	public function AvalaraGeneralException() {
		$this->exceptionType('AvalaraGeneralException');
	}
	
	
	
	//|| Public Functions ||//
	/**
	 * Returns the error message associated with this object.
	 */
	public function __toString() { return $this->message(); }
}



//|| Class: AvalaraTaxException
/**
 * Thrown when Avalara's server returns an error
 */
class AvalaraTaxException extends AvalaraGeneralException {
	//|| Data Members ||//
	public $result_code;
	
	
	
	//|| Gets / Sets ||//
	/**
	 * Gets/sets the exception's result code.
	 */
	public function resultCode($value = NULL) {
		if ($this->result_code !== NULL)
			$this->result_code = $value;
		return $this->result_code;
	}
	
	
	
	//|| Constructor ||//
	public function AvalaraTaxException() {
		$this->exceptionType('AvalaraTaxException');
	}
	
	
	
	//|| Public Methods ||//
	/**
	 * Sets the Avalara error code and message(s) associated with this error.
	 */
	public function setErrorData($result_code, $messages) {
		$this->resultCode($result_code);
		$this->message("An error occurred while trying to pull tax from Avalara's servers. (Result Code: $result_code)");
		foreach ($messages as $message) {
			$message = $message->getSummary().' :: '.$message->getDetails();
			$this->message($this->message()."\n - $message");
		}
	}
}



//|| Class: AvalaraAddressException
/**
 * Thrown when Avalara can't validate the address.
 */
class AvalaraAddressException extends AvalaraGeneralException {
	//|| Constructor ||//
	/**
	 * Builds the exception.
	 */
	public function AvalaraAddressException() {
		$this->exceptionType('AvalaraAddressException');
	}
}



//|| Class: AvalaraConnectionException
/**
 * Thrown when the servlet cannot connect with Avalara
 */
class AvalaraConnectionException extends AvalaraGeneralException {
	//|| Constructor
	/**
	 * Builds the exception.
	 */
	public function AvalaraConnectionException() {
		$this->exceptionType('AvalaraConnectionException');
	}
}



//|| Class: AvalaraServletRequest
/**
 * An object representing a servlet request which can be JSON encoded.
 */
class AvalaraServletRequest {
	public $request_type     = 0; // should be one of the defines listed at top of file
	public $request_object   = NULL;
	public $request_passcode = SERVLET_PASSCODE;
	//public $request_passcode = '';
	
	/**
	 * Constructs the request.
	 * 
	 * @param $request_type
	 *     An integer representing the type of request.  Valid values are
	 *     - REQUEST_TYPE_VALIDATE
	 *     - REQUEST_TYPE_TAX
	 * @param $request_object
	 *     A JSON-encodable object to be sent to the server
	 */
	public function AvalaraServletRequest($request_type, $request_object) {
		$this->request_type   = $request_type;
		$this->request_object = $request_object;
	}
}



//|| Class: AvalaraServletResponse
/**
 * Class designed to hold a json response from this servlet.
 */
class AvalaraServletResponse {
	public $request_type   = 0;
	public $errors         = false;
	public $exception      = NULL;
	public $request_object = NULL; // an object mapping to the data members of AvalaraInvoice
}
