<?php
/**
 * @copyright (C) 2004-2017 SICOM Systems Inc
 *            4434 Progress Meadow Drive
 *            Doylestown, PA 18902
 *            215-489-2500, FAX: 215-489-2769
 */

// 
//
// This file is used to handle requests to the Avalara servers.
// It is called by AvalaraAPI.php, and should be invisible to
// the application layer.
// 
// This servlet relies on php's JSON library
// 

//|| Includes
require_once('AvaTax4PHP/AvaTax.php');
require_once('Credentials.php');
require_once('AvalaraAPI.php');
require_once('logger.php');
require_once('dataobject.php');
// require_once('EmailSender.php');  

//|| Defines
define('REQUEST_THRESHOLD',   150                        ); // n requests per hour
define('BREACH_REPORT_FROM', 'From: dev <dev@sicom.com>' );
define('BREACH_REPORT_TO',   'dev@sicom.com'             );

//|| Functions
/**
 * Contains the main logic of the servlet.
 */
function main() {
	//:: Variables
	$request        = NULL;
	$response       = new AvalaraServletResponse();
	$passcode       = '';
	$valid_passcode = SERVLET_PASSCODE;
	
	//:: Inflate JSON
	$request  = json_decode(file_get_contents('php://input'));
	
	//:: Grab Passcode
	if ($request && isset($request->request_passcode))
		$passcode = $request->request_passcode;
	
	//:: Check Accurate JSON
	if (!$request) {
		$response->errors    = true;
		$response->exception = new AvalaraConnectionException();
		$response->exception->message("Invalid request; ensure that your JSON is properly encoded.");
	
	//:: Check Access
	//   this ensures that developers aren't inadvertantly contacting Avalara
	//   in a loop, building up crazy-large charges.  It also keeps crackers
	//   from doing the same.
	} else if (!accessOkay($request->request_type)) {
		$response->errors    = true;
		$response->exception = new AvalaraConnectionException();
		$response->exception->message("Too many requests are being made to Avalara; if you're a developer, check your code for loops.");
	
	//:: Perform Task
	} else if ($passcode == $valid_passcode) {
		switch ($request->request_type) {
			// test connection
			case REQUEST_TYPE_TEST:
				try {
					handlerTest($request, $response);
				} catch(AvalaraConnectionException $e) {
					$response->errors    = true;
					$response->exception = $e;
				} catch(Exception $e) {
					$response->errors    = true;
					$response->exception = new AvalaraGeneralException();
					$response->exception->message($e->getMessage());
				}
				break;
			
			// validate address
			case REQUEST_TYPE_VALIDATE:
				try {
					handlerValidate($request, $response);
				} catch(AvalaraAddressException $e) {
					$response->errors    = true;
					$response->exception = $e;
				} catch(SoapFault $e) {
					$response->errors    = true;
					$response->exception = new AvalaraConnectionException();
					$response->exception->message($e->getMessage());
				} catch(Exception $e) {
					$response->errors    = true;
					$response->exception = new AvalaraGeneralException();
					$response->exception->message($e->getMessage());
				}
				break;
			
			// calculate tax
			case REQUEST_TYPE_TAX:
				try {
					handlerTax($request, $response);
				} catch(AvalaraTaxException $e) {
					$response->errors    = true;
					$response->exception = $e;
				} catch(SoapFault $e) {
					$response->errors    = true;
					$response->exception = new AvalaraConnectionException();
					$response->exception->message($e->getMessage());
				} catch(Exception $e) {
					$response->errors    = true;
					$response->exception = new AvalaraGeneralException();
					$response->exception->message($e->getMessage());
				}
				break;
			
			// unsupported request type
			default:
				$response = new AvalaraServletResponse();
				$response->errors = true;
				$response->exception = new AvalaraGeneralException();
				$response->exception->message(sprintf(_("The request type '%s' is not supported by the servlet."), $request->request_type));
				break;
		} // end switch
	
	//:: Handle A Bad Passcode
	} else {
		//:: Log Problem
		log_error("ALERT - AvalaraAPIServlet has received an invalid passcode in a request: '$passcode'.");
		
		//:: Email Problem
		// $email_from    = BREACH_REPORT_FROM;
		// $email_address = BREACH_REPORT_TO;
		// $email_subject = _('ALERT: Possible security breach');
		// $email_message = _("
		
		$subject = 'ALERT: Possible security breach';
        $body    = <<<BODY
A possible breach of security may be transpiring in Utopia's Avalara API servlet.
A request has beensent to the servlet which has an invalid security passcode;
it is unlikely that any of our code would be mistakenly using an invalid passcode,
so this request may be coming from an outside cracker.

Valid Passcode: " . $valid_passcode . "
Provided Passcode: " . $passcode . "
BODY;
// ");
		// $email = new EmailSender($email_from, $email_address, $email_subject, $email_message);
		// $email->send();

		mail(BREACH_REPORT_TO, $subject, $body, BREACH_REPORT_FROM);
		
		//:: Build Response
		$response = new AvalaraServletResponse();
		$response->errors = true;
		$response->exception = new AvalaraGeneralException();
		$response->exception->message(_("An error has occurred.")); // don't give any help to crackers.
	}
	
	//:: Log Errors
	if ($response->errors)
		log_error('Avalara Servlet Error :: '.$response->exception->message());
	
	//:: Return Response
	echo json_encode($response);
}

/**
 * Determines that the number of requests in a given period of time are
 * below a reasonable threshold.
 * 
 * It also logs the current request into the access log
 */
function accessOkay($request_type) {
	//:: Prevent SQL Injection
	if (!is_numeric($request_type))
		$request_type = -1;
	
	//:: Update Access Log
	$dob = new DataObject('hand_sanitizer');
	$dob->insertUpdate("INSERT INTO avalara_access_log (access_type) VALUES ($request_type)");
	$dob->destroy();
	
	//:: Grab Number Of Requests In Last Hour
	$dob = new DataObject('picture_frame');
	$dob->setQuery("SELECT COUNT(avalara_access_log_uid) AS count FROM avalara_access_log WHERE access_time >= DATE_SUB(NOW(), INTERVAL 1 HOUR)");
	$dob->fetch();
	$num_requests = $dob->getValue('count');
	$dob->destroy();
	
	//:: Check If Access Threshold Has Been Breached
	$request_threshold = REQUEST_THRESHOLD;
	if ($num_requests < $request_threshold)
		return true;
	else {
		log_error("Too many requests are being made to Avalara (over $request_threshold in the last hour).");
		return false;
	}
}

/**
 * Handles a test request.
 * 
 * This doesn't presently test anything except the existence of a
 * connection to this servlet.  However there is room
 * 
 * @throws AvalaraConnectionException
 */
function handlerTest($request, $response) {
	//:: Inflate Test
	$test = new AvalaraTestServlet();
	$test->decloneJSON($request->request_object);
	
	//:: Test
	try {
		if (!$test->test()) {
			$exception = new AvalaraConnectionException();
			$exception->message(_("An error was encountered while trying to connect to Avalara's server."));
		}
	} catch(SoapFault $e) {
		$exception = new AvalaraConnectionException();
		$exception->message(_("An error was encountered while trying to connect to Avalara's server: ").$e->getMessage());
		throw $exception;
	}
	
	//:: Set Up Response
	$response->request_object = $test->cloneJSON();
	$response->request_type   = $request->request_type;
}

/**
 * Handles a validation request.
 * 
 * @throws AvalaraAddressException
 *     thrown if Avalara wasn't able to validate the address.
 * @throws SoapFault
 *     thrown if there was a problem connecting to Avalara's server.
 */
function handlerValidate($request, $response) {
	//:: Inflate Address
	$address = new AvalaraAddressServlet();
	$address->decloneJSON($request->request_object);
	
	//:: Validate Address
	try {
		$address->validate();
	} catch(Exception $e) {
		throw $e;
	}
	
	//:: Set Up Response
	$response->request_object = $address->cloneJSON();
	$response->request_type   = $request->request_type;
}

/**
 * Handles a tax calculation request.
 * 
 * @throws AvalaraTaxException
 *     thrown if Avalara encountered problems calculating tax.
 * @throws SoapFault
 *     thrown if there was a problem connecting to Avalara's server.
 */
function handlerTax($request, $response) {
	//:: Inflate Invoice
	$invoice = new AvalaraInvoiceServlet();
	$invoice->decloneJSON($request->request_object);
	
	//:: Validate Address
	try {
		$invoice->calculateTax();
	} catch(Exception $e) {
		throw $e;
	}
	
	//:: Set Up Response
	$response->request_object = $invoice->cloneJSON();
	$response->request_type   = $request->request_type;
}



//|| Class: AvalaraTestServlet
/**
 * Extends AvalaraTest, overriding the methods which contact this servlet with
 * methods which contact Avalara, if such a test is requested.
 * 
 * The reason for this design choice is because the communication between
 * this servlet and the client is little more than a JSON encoded AvalaraTest
 * object.  This helps keep the communication straightforward and maintainable,
 * however if the internal structure of AvalaraTest changes, this servlet
 * may need to be updated.  Wrapping the JSON in an AvalaraTest object on both
 * ends of the communication helps to ensure that no matter what changes in the
 * inner workings of the class, a consistent interface will remain functional.
 */
class AvalaraTestServlet extends AvalaraTest {
	//|| Constructor ||//
	/**
	 * Constructs the object.
	 */
	public function AvalaraTestServlet() {
		parent::AvalaraTest();
	}
	
	
	
	//|| Overridden Functions ||//
	/**
	 * Tests the connection with Avalara, if such a test was requested.
	 * 
	 * @throws SoapFault if it can't connect.
	 */
	public function test() {
		if ($this->checkAvalaraConnection()) {
			try {
				$address = new AvalaraAddressServlet();
				$address->productionMode($this->productionMode());
				$address->validate();
			} catch(SoapFault $e) {
				throw $e; // throw any connection exceptions
			} catch(Exception $e) {
				          // do nothing
			}
		}
		
		return true;
	}
}



//|| Class: AvalaraAddressServlet
/**
 * Extends AvalaraAddress, overriding the methods which contact this servlet with
 * methods which contact Avalara.
 * 
 * The reason for this design choice is because the communication between
 * this servlet and the client is little more than a JSON encoded AvalaraAddress
 * object.  This helps keep the communication straightforward and maintainable,
 * however if the internal structure of AvalaraAddress changes, this servlet
 * may need to be updated.  Wrapping the JSON in an AvalaraAddress object on both
 * ends of the communication helps to ensure that no matter what changes in the
 * inner workings of the class, a consistent interface will remain functional.
 */
class AvalaraAddressServlet extends AvalaraAddress {
	//|| Constructor ||//
	/**
	 * Constructs the object based on the provided JSON invoice clone.
	 */
	public function AvalaraAddressServlet() {
		parent::AvalaraAddress();
	}
	
	
	
	//|| Overridden Functions ||//
	/**
	 * Sends the address stored in this object to Avalara for validation.
	 * Upon receiving a response, it updates the stored address to the
	 * valid address.
	 * 
	 * @throws AvalaraAddressException
	 *     thrown if Avalara wasn't able to validate the address.
	 * @throws SoapFault
	 *     thrown if there was a problem connecting to Avalara's server.
	 */
	public function validate() {
		//:: Grab SOAP Client Object
		if ($this->productionMode())
			$client = new AddressServiceSoap('Production');
		else
			$client = new AddressServiceSoap('Development');
		
		//:: Build Request
		$address = new Address();
		$address->setLine1      ($this->address1   ());
		$address->setLine2      ($this->address2   ());
		$address->setLine3      ($this->address3   ());
		$address->setCity       ($this->city       ());
		$address->setRegion     ($this->region     ());
		$address->setPostalCode ($this->postalCode ());
		$address->setCountry    ($this->country    ());
		$request = new ValidateRequest($address);
		
		//:: Send Request
		try {
			$response = $client->validate($request);
		} catch (SoapFault $e) {
			throw $e;
		}
		
		//:: Validate
		$result_code = $response->getResultCode();
		if ($result_code != SeverityLevel::$Success) {
			$exception = new AvalaraAddressException();
			$exception->message(_("The provided address could not be validated."));
			throw $exception;
		}
		
		//:: Grab Valid Address
		$valid_addresses = $response->getValidAddresses();
		$valid_address   = NULL;
		
		$count = count($valid_addresses);
		if ($count < 1) {
			$exception = new AvalaraAddressException();
			$exception->message(_("No valid address candidates could be found."));
			throw $exception;
		} else
			$valid_address = $valid_addresses[0];
		
		//:: Store Values Of Valid Address
		$this->address1   ($valid_address->getLine1      ());
		$this->address2   ($valid_address->getLine2      ());
		$this->address3   ($valid_address->getLine3      ());
		$this->city       ($valid_address->getCity       ());
		$this->region     ($valid_address->getRegion     ());
		$this->postalCode ($valid_address->getPostalCode ());
		
		//:: Reset Flags
		$this->validated(true);
	}
}



//|| Class: AvalaraInvoiceServlet
/**
 * Extends AvalaraInvoice, overriding the methods which contact this servlet with
 * methods which contact Avalara.
 * 
 * The reason for this design choice is because the communication between
 * this servlet and the client is little more than a JSON encoded AvalaraInvoice
 * object.  This helps keep the communication straightforward and maintainable,
 * however if the internal structure of AvalaraInvoice changes, this servlet
 * may need to be updated.  Wrapping the JSON in an AvalaraInvoice object on both
 * ends of the communication helps to ensure that no matter what changes in the
 * inner workings of the class, a consistent interface will remain functional.
 */
class AvalaraInvoiceServlet extends AvalaraInvoice {
	//|| Constructor ||//
	/**
	 * Constructs the object based on the provided JSON invoice clone.
	 */
	public function AvalaraInvoiceServlet() {
		parent::AvalaraInvoice();
		$this->address = new AvalaraAddressServlet();
	}
	
	
	
	//|| Overridden Functions ||//
	/**
	 * Calculates the tax and stores it in the invoice.
	 * 
	 * @throws AvaleraTaxException
	 *     thrown if an error occurs while calculating tax.
	 * @throws SoapFault
	 *     thrown if there was a problem connecting to Avalara.
	 */
	public function calculateTax() {
		//:: Grab SOAP Client Object
		if ($this->productionMode())
			$client = new TaxServiceSoap('Production');
		else
			$client = new TaxServiceSoap('Development');
		
		//:: Build Request
		$request = new GetTaxRequest();
		
		//   Add Origin Address
		$origin = new Address();
		$origin->setLine1      (SICOM_ADDRESS);
		$origin->setCity       (SICOM_CITY   );
		$origin->setRegion     (SICOM_STATE  );
		$origin->setPostalCode (SICOM_ZIP    );
		$origin->setCountry    (SICOM_COUNTRY);
		$request->setOriginAddress($origin);
		
		//   Add Destination Address
		$destination = new Address();
		$destination->setLine1      ($this->address1   ());	
		$destination->setLine2      ($this->address2   ());	
		$destination->setLine3      ($this->address3   ());
		$destination->setCity       ($this->city       ());
		$destination->setRegion     ($this->region     ());
		$destination->setPostalCode ($this->postalCode ());
		$destination->setCountry    ($this->country    ());
		$request->setDestinationAddress($destination);
		
		//   Add Other Stuff
		$request->setCompanyCode(MAS_COMPANY); // main company in MAS 90
		$request->setDocType(DocumentType::$SalesOrder);
		
		$date_time = time();
		$docCode   = "AvalaraAPI.php_".date("dmyGis", $date_time);
		
		$request->setDocCode($docCode);                            // invoice number
		$request->setDocDate(date("Y-m-d", $this->invoiceDate())); // date
		$request->setSalespersonCode("");                          // string Optional
		$request->setCustomerCode($this->accountCode());           // string Required
		$request->setCustomerUsageType("");                        // string Entity Usage
		$request->setDiscount(0.00);                               // decimal
		$request->setDetailLevel(DetailLevel::$Tax);               // Summary or Document or Line or Tax or Diagnostic
		
		//:: Build Line Items
		$lines = array();
		for ($index = 0, $length = $this->countLineItems(); $index < $length; $index++) {
			// grab line item
			$line_item = $this->getLineItem($index);
			
			// build avatax line item
			$quantity = $line_item->quantity();
			$amount   = $quantity * ($line_item->price() / 100); // convert from SICOM's integer representation to decimal
			$line = new Line();
			$line->setNo          ($line_item->index()      ); // string - line number of invoice
			$line->setItemCode    ($line_item->partNumber() ); // string
			$line->setDescription ($line_item->description()); // string
			$line->setQty         ($quantity                ); // decimal
			$line->setAmount      ($amount                  ); // decimal - total amount
			$line->setDiscounted  (false                    ); // boolean
			
			// add to array
			$lines[] = $line;
		}
		$request->setLines($lines);
		
		//:: Send Request
		try {
			$tax_result  = $client->getTax($request);
			$result_code = $tax_result->getResultCode();
			
			// success
			if ($result_code == SeverityLevel::$Success) {
				// set total tax
				$this->setTax(intval(round((((float) $tax_result->getTotalTax()) * 100), 2)));

				// set line item tax breakdown
				$tax_lines = $tax_result->getTaxLines();
				foreach ($tax_lines as $tax_line) {
					// grab correct line item
					$line_item = $this->getLineItem($tax_line->getNo());
					
					// update line item
					$line_item->updateTax($tax_line);
				}
			
			// failure
			} else {
				$exception = new AvalaraTaxException();
				$exception->setErrorData($result_code, $tax_result->getMessages());
				throw $exception;
			}
		} catch (SoapFault $e) {
			throw $e;
		}
		
		//:: Reset Dirty Flag
		$this->modified(false);
		for ($i = 0, $len = $this->countLineItems(); $i < $len; $i++)
			$this->getLineItem($i)->modified(false);
	}
}



//|| Begin
main();
?>
