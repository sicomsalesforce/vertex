<?php
/******************************************************************************
 *
 * COPYRIGHT (C) 2009 SICOM Systems Inc
 * 4140 Skyron Drive
 * Doylestown, PA 18902
 * 215-489-2500, FAX: 215-489-2769
 *
 * $Id: urlstack.php,v 1.5 2010/10/25 21:22:12 kedupuganti Exp $
 *
 ******************************************************************************/

class UrlStack
{
	/************************
	 *** Member Variables ***/

	var $urlstack	= array();
	var $namestack	= array();
	var $helpstack	= array();
	var $appstack	= array();

	/*** End Member Variables ***
	 ****************************/

	function push($url, $name, $help, $app)
	{
		$this->urlstack[count($this->urlstack)]		= $url;
		$this->namestack[count($this->namestack)]	= $name;
		$this->helpstack[count($this->helpstack)]	= $help;
		$this->appstack[count($this->appstack)]		= $app;
	}

	function showStack()
	{
		print _("NAME STACK")."<br />\n";

		foreach ($this->namestack as $x => $y)
		{
			print "$x ---> $y <br />\n";
		}

		print _("URL STACK")."<br />\n";

		foreach ($this->urlstack as $x => $y)
		{
			print "$x ---> $y <br />\n";
		}

		print _("APP STACK")."<br />\n";

		foreach ($this->appstack as $x => $y)
		{
			print "$x ---> $y <br />\n";
		}
	}

	function size()
	{
		return count($this->urlstack);
	}

	function getName()
	{
		if (count($this->namestack)-2 < 0)
			return '';

		return $this->namestack[count($this->namestack)-2];
	}

	function getURL()
	{
		if(count($this->urlstack)-2 < 0)
			return '';

		return $this->urlstack[count($this->urlstack)-2];
	}

	function getURL2()
	{
		return $this->urlstack[count($this->urlstack)-1];
	}

	function setCurrentURL($url)
	{
		$this->urlstack[count($this->urlstack)-1] = $url;
	}

	function getHelp()
	{
		if(count($this->helpstack)-2 < 0)
			return '';

		return $this->helpstack[count($this->helpstack)-2];
	}

	function getApp()
	{
		if(count($this->appstack)-2 < 0)
			return '';

		return $this->appstack[count($this->appstack)-2];
	}

	function pop()
	{
		unset($this->urlstack[count($this->urlstack)-1]);
		unset($this->namestack[count($this->namestack)-1]);
		unset($this->helpstack[count($this->helpstack)-1]);
		unset($this->appstack[count($this->appstack)-1]);
	}
}
?>
