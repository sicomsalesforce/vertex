<?php
/******************************************************************************
 *
 * COPYRIGHT (C) 2009 SICOM Systems Inc
 * 4140 Skyron Drive
 * Doylestown, PA 18902
 * 215-489-2500, FAX: 215-489-2769
 *
 * $Id: logger.php,v 1.20 2010/10/25 21:22:12 kedupuganti Exp $
 *
 ******************************************************************************/
require_once("siteunique.php");

define('CLOG_USESESSION', 0);
define('CLOG_FATAL', 1);
define('CLOG_ERROR', 2);
define('CLOG_WARNING', 3);
define('CLOG_INFO', 4);
define('CLOG_DEBUG', 5);
define('CLOG_LOGIN', 6);
define('CLOG_DO_QUERY', 7);
define('CLOG_DO_UPDATE', 8);
define('CLOG_DO_INSERT', 9);
define('CLOG_DO_DELETE', 10);
define('CLOG_DO_CONSTRUCT', 11);
define('CLOG_SET_AJAX_WARN', 12);
define('CLOG_SET_AJAX_ERROR', 13);


$loggerpath = $__sicom__log_path;
$loggerflags = '01111010000000000000000000000000';
if (isset($__sicom__log_flags)) {
	$loggerflags = $__sicom__log_flags;
}


class Logger
{
	var $logfile;
	var $loggerflags;
	
	function Logger() {
		global $loggerflags;
		$this->loggerflags = $loggerflags;
	}
	
	
	function log($type, $msg)
	{
		global $loggerpath;
		
		$appid	= '-NONE-';
		$mt		= microtime();
		list($usecs, $secs) = explode(' ',$mt);
		$usecs = sprintf("%06d", floor($usecs * 1000000));
		if(isset($_SESSION)) {
			if ((substr($this->loggerflags, CLOG_USESESSION, 1) > 0) && isset($_SESSION['DISABLE_LOGGER']) ) {
				return;
			}
			if (array_key_exists('appid', $_SESSION)) {
				$appid = sprintf('%6d', $_SESSION['appid']);
			}
		}
		$sessionid = '00';
		if(isset($_SESSION) && array_key_exists('requestid', $_SESSION))
		{
			$sessionid = sprintf('%02d', $_SESSION['requestid']%100);
		}

		if (is_null($this->logfile))
		{
			global $__sicom__log_name;
			if(!isset($__sicom__log_name))
				$fname	= $loggerpath . 'sems.log';
			else
				$fname	= $loggerpath . $__sicom__log_name;
			$this->logfile = fopen($fname, "a");
		}

		if (isset($this->logfile))
		{
			fwrite($this->logfile, date("YmdHis").'.'.$usecs.' '.$appid.'-'.$sessionid.' '.$type.' '.$msg."\n");
		}
	}
}


/**
** log error message
*/
function log_error($msg) {
	global $__system_logger;
	if (substr($__system_logger->loggerflags, CLOG_ERROR, 1) > 0) {
		$__system_logger->log("ERROR  ", $msg);
		if (substr($__system_logger->loggerflags, CLOG_SET_AJAX_ERROR, 1) > 0) {
			if(isset($GLOBALS['AJAX_ENVIRONMENT'])) {
				log_debug(print_r(debug_backtrace(),TRUE));

				if ($_SESSION['access'] < 99) {
					$msg = _('Call SICOM Systems 1.800.547.4266')."\n\n";
					$msg .= _("Please be prepared to provide the following information:")."\n\n";
					$msg .= _(" Your Name,")."\n";
					$msg .= _(" Your Company Name,")."\n";
					$msg .= _(" Your Restaurant's Telephone Number, City, and State,")."\n";
					$msg .= _(" And, a brief description of what action was performed prior to this message.")."\n\n";
					$msg .= _("Thank you!")."\n";
				}

				$GLOBALS['AJAX_ENVIRONMENT_ERRORS'][] = $msg; 
			}
		}
	}
}


/**
** log warning messages
*/
function log_warning($msg) {
	global $__system_logger;
	if (substr($__system_logger->loggerflags, CLOG_WARNING, 1) > 0) {
		$__system_logger->log("WARNING", $msg);
		if (substr($__system_logger->loggerflags, CLOG_SET_AJAX_WARN, 1) > 0) {
			if(isset($GLOBALS['AJAX_ENVIRONMENT'])) {
				$GLOBALS['AJAX_ENVIRONMENT_ERRORS'][] = $msg;
			}
		}
	}
}


/**
** log debug messages.
*/
function log_debug($msg) {
	global $__system_logger;
	if (substr($__system_logger->loggerflags, CLOG_DEBUG, 1) > 0) {
		$__system_logger->log("DEBUG  ", $msg);
	}
}


/**
** Log information messages in the log.
*/
function log_info($msg) {
	global $__system_logger;
	if (substr($__system_logger->loggerflags, CLOG_INFO, 1) > 0) {
		$__system_logger->log("INFO   ", $msg);
	}
}


/**
** Log other messages. 
** log_flag controls whether or not message is issued.
** logtag is the tag to display in the log file
** msg is the logged message
*/
function log_other($log_flag, $logtag, $msg) {
	global $__system_logger;
	if (should_log($log_flag)) {
		if (strlen($logtag > 7)) $logtag = substr($logtag, 0, 7);
		while (strlen($logtag) < 7) $logtag .= ' '; 
		$__system_logger->log($logtag, $msg);
	}
}


/**
** Log a message about a FATAL error to the log file, then exit to a safe place
*/
function log_fatal($msg) {
	global $__system_logger;
	if (substr($__system_logger->loggerflags, CLOG_FATAL, 1) > 0) {
		$__system_logger->log("FATAL  ", $msg);
		$tmsg = urlencode($msg);
		if(!isset($GLOBALS['AJAX_ENVIRONMENT'])) {
			$l = 0;
			if( isset($_SESSION['access']) ) 
				$l = $_SESSION['access']; // good for both mgrng & sems

			header("location: callsicom.php?ERROR=$tmsg&l=$l\n");
			exit(1);
		} else {
			$GLOBALS['AJAX_ENVIRONMENT_ERRORS'][] = $msg;
		}
	}                                                                                                            
}


/**
** Should we log this flag numbers messages? returns true/false.
*/
function should_log($flagnum) {
	global $__system_logger;
	$flags = $__system_logger->loggerflags;
	$flag = substr($flags, $flagnum, 1);
	$result = ($flag > 0);
	return $result;
}

//$__system_logger = new Logger();
$__system_logger = new Logger();
?>
